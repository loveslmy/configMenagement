package com.zk.config.web.op;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.zkclient.ZkClient;
import com.zk.config.api.util.ZkOperate;
import com.zk.config.web.model.ZkData;
import com.zk.config.web.util.Permiss;

public class Zk {
   private static final Logger logger = LoggerFactory.getLogger(Zk.class);
   // 192.168.161.61:2181,192.168.161.83:2181
   private ZkClient client;
   private Permiss permiss = new Permiss();

   public boolean exists(String path) {
      if (path == null || path.trim().equals("")) {
         throw new IllegalArgumentException("path can not be null or empty");
      }
      return getClient().exists(getPath(path));
   }

   public ZkData readData(String path) {
      ZkData zkdata = new ZkData();
      Stat stat = new Stat();
      zkdata.setData(getClient().readData(getPath(path), stat));
      zkdata.setStat(stat);
      return zkdata;
   }

   public List<String> getChildren(String path) {
      return getClient().getChildren(getPath(path));
   }

   public void create(String path, byte[] data, boolean permissOpen) {
      path = getPath(path);
      if(permissOpen) {
    	  List<ACL> acls = permiss.getACLList();
    	  if(acls.size() > 0) {
    		  ZkOperate.createNode(getClient().getZooKeeper(), path, data, acls);
    	  }
      } else {
    	  getClient().createPersistent(path, true);  
          if (data != null) {
        	  Stat stat = getClient().writeData(path, data);
        	  logger.info("create: node:{}, stat{}:", path, stat);
          }
      }
   }

   public void edit(String path, byte[] data) {
      path = getPath(path);
      Stat stat = getClient().writeData(path, data);
      logger.info("edit: node:{}, stat{}:", path, stat);
   }

   public void delete(String path) {
      path = getPath(path);
      boolean del = getClient().delete(path);
      logger.info("delete: node:{}, boolean{}:", path, del);
   }

   public void deleteRecursive(String path) {
      path = getPath(path);
      boolean deleteRecursive = getClient().deleteRecursive(path);
      logger.info("rmr: node:{}, boolean{}:", path, deleteRecursive);
   }

   public Zk(String cxnString) {
      logger.info("cxnString:{}", cxnString);
      this.client = ClientCacheManager.getClient(cxnString);
   }

   public ZkClient getClient() {
      return client;
   }

   public void setClient(ZkClient client) {
      this.client = client;
   }

   public String getPath(String path) {
      path = path == null ? "/" : path.trim();
      if (!StringUtils.startsWith(path, "/")) {
         path = "/" + path;
      }
      return path;
   }
}
