package com.zk.config.web.controller;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.zk.config.web.constants.Constants;
import com.zk.config.web.util.AuthUtils;
import com.zk.config.web.util.ComUtil;

@Controller
@RequestMapping("")
public class LoginController {
	
	@Value("${zookeeper.safe.permiss.open}")
	public boolean permissOpen = false;

	/*
	 * @RequestMapping("/login") public String login() { return "login"; }
	 */

	@RequestMapping("/login")
	public String login(HttpServletRequest request, String username, String password) {
		String cxnstr = (String) request.getSession().getAttribute(Constants.CX_STR);
		AuthUtils.login(username, password, cxnstr, permissOpen);
		String referer = request.getHeader("referer");
		String cp = request.getContextPath();
		return "redirect:" + StringUtils.substringAfter(referer, cp);
	}

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		String cxnstr = (String) request.getSession().getAttribute(Constants.CX_STR);
		AuthUtils.logout(cxnstr, permissOpen);
		String referer = request.getHeader("referer");
		String cp = request.getContextPath();
		return "redirect:" + StringUtils.substringAfter(referer, cp);
	}

}
