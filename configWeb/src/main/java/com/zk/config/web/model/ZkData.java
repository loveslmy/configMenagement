package com.zk.config.web.model;

import java.util.Arrays;
import org.apache.zookeeper.data.Stat;
import com.zk.config.web.constants.Constants;

public class ZkData {

   private byte[] data;
   private Stat stat;

   public byte[] getBytes() {
      return data;
   }

   @Override
   public String toString() {
      return "ZkData [data=" + Arrays.toString(getData()) + ",stat=" + getStat() + "]";
   }

   public String getDataString() {
	   if(getData() != null)
			return new String(getData(), Constants.defualtCharset);
	   return null;
   }
   
   public byte[] getData() {
      return data;
   }

   public void setData(byte[] data) {
      this.data = data;
   }

   public Stat getStat() {
      return stat;
   }

   public void setStat(Stat stat) {
      this.stat = stat;
   }
}
