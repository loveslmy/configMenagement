package com.zk.config.web.controller;

import java.util.Properties;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.zk.config.api.client.ConfigClient;
import com.zk.config.api.service.ZkCache;
import com.zk.config.api.test.Test;
import com.zk.config.web.util.ConfUtils;

@Controller
@RequestMapping("")
public class IndexController {
//	@Value("${user}")
//	private String user;
//	@Value("${userName}")
//	private String userName;
//	@Resource
//	private Properties configProperties;
//	@Resource
//	private ConfigClient configClient;
//	@Resource
//	private ConfigClient configClient2;
//	@Resource
//	private Test test;
   @RequestMapping(value = { "", "/", "/index" })
   public ModelAndView index() {
		// 这里是测试注入的代码
//		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$user="+ user + ",userName=" + userName);
		// 这里是测试注入的动态变化的值的代码
//		System.out.println("#######configProperties######user="+configProperties.getProperty("user") + ",userName=" + configProperties.getProperty("userName"));
//		System.out.println("#######ConfigClient.zkCache######user="+ZkCache.getPropertiesValue("user") + ",userName=" + ZkCache.getPropertiesValue("userName"));
//		System.out.println("#######configClient configClient2######user="+configClient.getZkCache().getPropertiesValue("user") + ",userName=" + configClient2.getZkCache().getPropertiesValue("userName"));
//		test.init();
		
      ModelAndView mav = new ModelAndView("index");
      mav.addObject("addrs", ConfUtils.getConxtions());
      return mav;
   }

}
