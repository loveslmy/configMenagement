package com.zk.config.web.model;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class TreeData {
	private String text;
	private String icon = "folder";
	private List<TreeData> children;
	private Map<String, String> state;
}
