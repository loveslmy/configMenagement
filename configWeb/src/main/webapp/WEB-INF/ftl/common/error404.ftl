<!DOCTYPE html>
<html>
	<head>
		<title>Zookeeper-Web</title>
		<script src="${host}/js/jquery-2.1.4.min.js" type="text/javascript"></script>
		<link href="${host}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="${host}/css/zk-web.css" rel="stylesheet" type="text/css">
	</head>
	<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-template">
                <h2>404 Not Found</h2>
                <div class="error-actions">
                    <a href="${host}" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span> 回主页 </a>
                </div>
            </div>
        </div>
    </div>
</div>

	</body>
</html>