<script type="text/javascript">
	$("#textAreaData2").val('${data?default("")?js_string}');
</script>

<div class="row-fluid">
	<div class="span12">
		<h3>节点状态</h3>
		<table class="table table-condensed table-hover table-striped">
				<tbody>
				<#if stat??>
				<tr>
				<#list stat?keys as key>
					<#if key_index%2 == 0 && key_index != 0>
					</tr><tr>
					</#if>
					<td>${key}</td>
					<#if key=="ctime" || key="mtime">
					<td>${stat[key]?datetime}</td>
					<#else>
					<td>${stat[key]}</td>
					</#if>
				</#list>
				</tr>
				</#if>
				</tbody>
			</table>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<h3>节点数据<span class="label label-info fontsize11">${dataSize!'0'} byte(s)</span></h3>
		<div class="well marginright50" style="overflow:auto;">
			<p style="word-break:break-all;">${data?default("")?html?replace("\n", "<br/>")}</p>
		</div>
	</div>
</div>