
### 基于Zookeeper的工程文件管理配置中心

在大型集群和分布式应用中，配置不宜分散到集群结点中，或者工程数量较多管理混乱，这时配置应该集中管理，这个配置中心基于使用简单，功能完善为目标，是个轻量级的配置中心，当配置有变化了，无需重启服务，变化了的值会自动下发给各个在使用的服务上，配置数据存于本地缓存，因为是访问内存所以速度很快，有定时刷新缓存和动态监听值的变化来刷新缓存两种方式。同时通过缓存保证了程序运行中zk挂了也不影响服务运行，当然zk号称不会挂的。节点可使用安全性管理，一个用户创建的节点，只有授权的用户可以访问，只有这个用户有权限编辑修改(不过可以被任何人删除)。
工程有api和web管理界面组成，由java开发，maven编译，有任何疑问欢迎指正。

邮箱：hezui@163.com

QQ：379049886

设计框架：springmvc、freemarker、bootstrap和zkclient


### 一、本地编译好confiApi的工程后，就可以在工程中导入api开始使用：

**1、maven包导入**
```
<dependency>
	<groupId>com.zk.config.api</groupId>
	<artifactId>configApi</artifactId>
	<version>0.0.1-SNAPSHOT</version>
</dependency>
```
**2、spring配置**
```
	<bean id="configProperties" class="com.zk.config.api.factory.CustomizedPropertyConfigurer">
		<property name="configClient">
			<list>
				<ref bean="configClient" /> <!-- 导入zookeeper里节点后缀为.properties的文件-->
			</list>
		</property>
	</bean>
	<bean id="configClient" class="com.zk.config.api.client.ConfigClient" scope="singleton" init-method="init">
		<constructor-arg index="0" value="192.168.3.6:2181,192.168.3.7:2181/configManagement/configWeb/dev" /> <!-- zookeeper的Host+配置管理根目录名+工程名+分支(如：dev,test,prod) -->
		<constructor-arg index="1" value="30000" /> <!-- zookeeper初始化连接超时参数connectTimeout，单位毫秒 -->
		<constructor-arg index="2" value="0" /> <!-- 定时刷新缓存数据的时间间隔，单位毫秒，0是不刷新  -->
		<constructor-arg index="3" value="true" /> <!-- 是否打开对节点的动态监听 -->
		<constructor-arg index="4" ref="configProperties" /> <!-- 支持动态取值的Properties -->
		<constructor-arg index="5" value="guest:guest123" /> <!-- 管理界面打开节点安全性管理之后，api需要提供用户:密码才能访问到数据 -->
	</bean>
```
**3、接下来可以像spring导普通属性文件一样简洁的使用zookeeper上的配置数据了。**
如：spring配置文件中的bean可以使用${user}这样的常规注入方式注入zk上的值，如下所示：
```
<bean id="test" class="com.zk.config.api.test.Test" init-method="init" scope="prototype">
	<property name="user" value="${user}" />
	<property name="userName" value="${userName}" />
</bean>
```
同样类文件中也可以这样注入
![输入图片说明](http://git.oschina.net/uploads/images/2015/1017/040802_f7fc6474_385094.png "在这里输入图片标题")

注意:

1、普通注入的值是创建对象后一次性缓存的值，要使用监听的动态变化的值，请使用注入的Properties对象获取（如这里定义的configProperties），或者直接通过静态缓存对象获取ZkCache.getPropertiesValue(key)。ZkCache是最早被初始化的静态对象，项目里的其他静态对象可以直接获取到相关的缓存内容，多个client的时候，key或者节点路径相同的则最后一个client的配置有效。

2、zookeeper上名称带.properties后缀的节点会自动识别成Properties文件，如果xml格式的properties文件可以在节点后加.properties后缀，支持识别，其他后缀的都做普通文件处理，通过ZkCache.getFileValueByNodePath(nodePath)来获取节点文件内容，nodePath是节点的相对路径。

3、zookeeper上的节点名以log4j.properties为后缀的日志配置文件将会默认自动装载，如果是其他日志文件如log4j.xml，请把configClient对象的log4jPathName属性值设为该文件名，如果要取消自动装载，设空串，如：
```
<property name="log4jPathName" value="" />
```
4、以冒号（:）开头的节点或目录将自动映射到本地的classpath路径下,让工程运行时调用。

### 二、web管理界面

这实际是zookeeper的web管理界面，对节点采用树形文件管理，包含备份还原功能和节点安全处理。
注意要先编译configApi然后关闭此工程，再启动configWeb的服务。
如果要使用节点安全性管理：请将配置文件config.properties中的配置设置为zookeeper.safe.permiss.open=true，默认值是false做普通web管理，设为true之后创建的节点需用户密码授权才能操作。

![输入图片说明](http://git.oschina.net/uploads/images/2015/1130/021432_1545023d_385094.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2015/1130/021448_968b0440_385094.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2015/1130/021502_8c5266b2_385094.png "在这里输入图片标题")

### 问题和建议

邮箱：hezui@163.com

QQ：379049886