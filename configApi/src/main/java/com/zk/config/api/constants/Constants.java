package com.zk.config.api.constants;

import java.nio.charset.Charset;

public class Constants {
	
	public static int connectTimeout = 5000;
	
	public static String zkHost = "";
	
	public static long refreshDataTime = 0;
	
	public static boolean watch = false;
	
	public static String authInfo = "";
	
	public static String zkRoot = "/";
	
	public static String log4jPathName = "log4j.properties";
	
	public static Charset defualtCharset = Charset.forName("UTF-8");
	
	public static String mapingToLocalTag = ":";
	
	// Constants.class.getResource("/").getPath();
	public static String mapingDir = Thread.currentThread().getContextClassLoader().getResource("").getPath();
}
