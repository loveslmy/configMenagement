package com.zk.config.api.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.auth.DigestAuthenticationProvider;
import org.springframework.util.CollectionUtils;
import com.zk.config.api.client.MappingFile;
import com.zk.config.api.constants.Constants;
import com.zk.config.api.util.ComUtil;
import com.zk.config.api.util.ZkCom;
import com.zk.config.api.util.ZkOperate;
import lombok.Setter;

public class ZkCache {
	private static Logger logger = Logger.getLogger(ZkCache.class);
	@Setter
	private ZooKeeper zk;
	public static Map<String, String> fileValue = new HashMap<>();
	public static Properties properties;
	public static Map<String, Properties> fileProperties = new HashMap<>();
	
	public ZkCache() {
	}
	
	public ZkCache(ZooKeeper zk) {
		this.zk = zk;
	}
	
	public void init() {
		read(Constants.zkRoot, Constants.watch);
	}
	
	public void refresh(){
		if(zk == null) {
			return;
		}
		Map<String, String> fileValue = new HashMap<>();
		Map<String, Properties> fileProperties = new HashMap<>();
		Properties properties = new Properties();
		read(fileValue, fileProperties, properties, Constants.zkRoot, Constants.watch);
		if (properties.size() > 0 && ZkCache.properties != null) {
			List<Object> removekeys = new ArrayList<>();
			for(Object key:ZkCache.properties.keySet()) {
				if(!properties.containsKey(key)) {
					removekeys.add(key);
				}
			}
			ZkCache.properties.putAll(properties);
			for(Object key:removekeys) {
				ZkCache.properties.remove(key);
			}
			properties.clear();
			properties = null;
			removekeys.clear();
			removekeys = null;
		} else {
			properties.clear();
			properties = null;
		}
		if (fileProperties.size() > 0) {
			Map<String, Properties> fileProperties2 = ZkCache.fileProperties;
			ZkCache.fileProperties = fileProperties;
			fileProperties2.clear();
			fileProperties2 = null;
		} else {
			fileProperties.clear();
			fileProperties = null;
		}
		if (fileValue.size() > 0) {
			Map<String, String> fileValue2 = ZkCache.fileValue;
			ZkCache.fileValue = fileValue;
			fileValue2.clear();
			fileValue2 = null;
		} else {
			fileValue.clear();
			fileValue = null;
		}
	}
	
	public void reloadWatcher(String path){
		String zkPath = getZkPath(path);
		if (ZkOperate.existsNode(zk, zkPath, Constants.watch) != null) {
			List<String> childrens = ZkOperate.getChildren(zk, zkPath, Constants.watch);
			if (childrens != null && childrens.size() > 0) {
				zkPath = "/".equals(zkPath)?"/":(zkPath+"/");
				for(String c:childrens) {
					reloadWatcher(zkPath+c);
				}
			}
		}
	}
	
	public void read(Map<String, String> fileValue, Map<String, Properties> fileProperties, Properties properties, String path, boolean watch){
		String zkPath = getZkPath(path);
		if (ZkOperate.existsNode(zk, zkPath, watch) != null) {
			String data = ZkOperate.getNodeData(zk, zkPath);
			List<String> childrens = ZkOperate.getChildren(zk, zkPath, watch);
			if(ComUtil.isNotEmpty(Constants.mapingToLocalTag) && zkPath.indexOf("/"+Constants.mapingToLocalTag) != -1) {
				String filePathName = zkPath.replaceAll("/"+Constants.mapingToLocalTag, "/");
				String filePath = Constants.mapingDir+filePathName;
				if(childrens == null || childrens.size() == 0) {
					if(data != null && data.length() > 0) {
						String args[] = {filePath, String.valueOf(true), data};
						MappingFile.main(args);
					} else {
						String args[] = {filePath, String.valueOf(false), data};
						MappingFile.main(args);
					}
				} else {
					String args[] = {filePath, String.valueOf(false), data};
					MappingFile.main(args);
				}
			} else {
				if (data != null && data.length() > 0) {
					boolean isLog4jFile = false;
					if(ComUtil.isNotEmpty(Constants.log4jPathName)
							&& zkPath.endsWith(Constants.log4jPathName)) {
						isLog4jFile = true;
					}
					if (zkPath.endsWith(".properties") || isLog4jFile) {
						Properties p = new Properties();
						try {
							p.load(new ByteArrayInputStream(data.getBytes(Constants.defualtCharset)));
						} catch (IOException e) {
							e.printStackTrace();
							logger.error("It isnt a valid properties file format.");
							return;
						}
						if(isLog4jFile) {
							PropertyConfigurator.configure(p);
						} else {
							if (properties != null) {
								properties.putAll(p);
							}
						}
						
						if(fileProperties != null) {
							fileProperties.put(zkPath, p);
						}
					} else {
						if(fileValue != null) {
							fileValue.put(zkPath, data);
						}
					}
				}
			}
			
			if (childrens != null && childrens.size() > 0) {
				zkPath = "/".equals(zkPath)?"/":(zkPath+"/");
				for(String c:childrens) {
					read(fileValue, fileProperties, properties, zkPath+c, watch);
				}
			}
		}
	}
	
	public void read(String path, boolean watch) {
		read(fileValue, fileProperties, properties, path, watch);
	}
	
	public void reloadIfNodeDelete(String path){
		String zkPath = getZkPath(path);
		if(ComUtil.isNotEmpty(Constants.mapingToLocalTag) && zkPath.indexOf("/"+Constants.mapingToLocalTag) != -1) {
			
		} else {
			Map<String, String> fileValue = new HashMap<>();
			Map<String, Properties> fileProperties = new HashMap<>();
			fileValue.putAll(ZkCache.fileValue);
			String newZkPath = ZkCom.getZkEndPath(zkPath);
			for(String key:ZkCache.fileValue.keySet()) {
				if(ZkCom.getZkEndPath(key).startsWith(newZkPath)) {
					fileValue.remove(key);
				}
			}
			if(fileValue.containsKey(zkPath)) {
				fileValue.remove(zkPath);
			}
			fileProperties.putAll(ZkCache.fileProperties);
			for(String key:ZkCache.fileProperties.keySet()) {
				if(ZkCom.getZkEndPath(key).startsWith(newZkPath)) {
					removeProperties(fileProperties, key);
					fileProperties.remove(key);
				}
			}
			if(fileProperties.containsKey(zkPath)) {
				removeProperties(fileProperties, zkPath);
				fileProperties.remove(zkPath);
			}
			
			Map<String, String> fileValue2 = ZkCache.fileValue;
			Map<String, Properties> fileProperties2 = ZkCache.fileProperties;
			ZkCache.fileValue = fileValue;
			ZkCache.fileProperties = fileProperties;
			fileValue2.clear();
			fileValue2 = null;
			fileProperties2.clear();
			fileProperties2 = null;
		}
	}
	
	private void removeProperties(Map<String, Properties> fileProperties, String key){
		if(ZkCache.properties == null) return;
		Properties properties = fileProperties.get(key);
		if (properties != null && properties.size() > 0) {
			for(Object k:properties.keySet()) {
				Object val = properties.get(k);
				Object val2 = ZkCache.properties.get(k);
				if (val != null && val.equals(val2)) {
					ZkCache.properties.remove(k);
				}
			}
		}
	}
	
	public void reloadIfNodeCreateOrChange(String path){
		String zkPath = getZkPath(path);
		String data = ZkOperate.getNodeData(zk, zkPath);
		if(ComUtil.isNotEmpty(Constants.mapingToLocalTag) && zkPath.indexOf("/"+Constants.mapingToLocalTag) != -1) {
			String filePathName = zkPath.replaceAll("/"+Constants.mapingToLocalTag, "/");
			List<String> childrens = ZkOperate.getChildren(zk, zkPath, false);
			String filePath = Constants.mapingDir+filePathName;
			if(childrens == null || childrens.size() == 0) {
				if(data != null && data.length() > 0) {
					String args[] = {filePath, String.valueOf(true), data};
					MappingFile.main(args);
				} else {
					String args[] = {filePath, String.valueOf(false), data};
					MappingFile.main(args);
				}
			} else {
				String args[] = {filePath, String.valueOf(false), data};
				MappingFile.main(args);
			}
		} else {
			if (data != null && data.length() > 0) {
				boolean isLog4jFile = false;
				if(ComUtil.isNotEmpty(Constants.log4jPathName)
						&& zkPath.endsWith(Constants.log4jPathName)) {
					isLog4jFile = true;
				}
				if (zkPath.endsWith(".properties") || isLog4jFile) {
					Properties p = new Properties();
					try {
						p.load(new ByteArrayInputStream(data.getBytes(Constants.defualtCharset)));
					} catch (IOException e) {
						e.printStackTrace();
						logger.error("It isnt a valid properties file format.");
						return;
					}
					if(isLog4jFile) {
						PropertyConfigurator.configure(p);
					} else {
						if(properties != null) {
							properties.putAll(p);
						}
						Properties fp = fileProperties.get(zkPath);
						if(fp != null) {
							for(Object key:fp.keySet()) {
								if(!p.containsKey(key)) {
									properties.remove(key);
								}
							}
						}
					}
					if(fileProperties != null) {
						fileProperties.put(zkPath, p);
					}
				} else {
					if(fileValue != null) {
						fileValue.put(zkPath, data);
					}
				}
			}
		}
	}
	
	public static String getPropertiesValue(String key){
		if (properties != null && properties.size() > 0) {
			return properties.getProperty(key);
		}
		return null;
	}
	
	public static Properties getPropertiesByNodePath(String ...nodePath){
		if (fileProperties.size() > 0) {
			return fileProperties.get(getZkPath(nodePath));
		}
		return null;
	}
	
//	public static String getPropertiesValue(String nodePath, String key){
//		String filePath = getZkPath(nodePath);
//		if (fileProperties.containsKey(filePath)) {
//			Properties pro = fileProperties.get(filePath);
//			if(pro.containsKey(key)) {
//				return pro.getProperty(key);
//			}
//		}
//		return null;
//	}
	
	public static String getFileValueByNodePath(String ...nodePath){
		if (fileValue.size() > 0) {
			return fileValue.get(getZkPath(nodePath));
		}
		return null;
	}
	
	private List<ACL> getCurrentACLList() {
		List<ACL> acls = new ArrayList<ACL>();
		try {
			Id id = new Id("digest", DigestAuthenticationProvider.generateDigest(Constants.authInfo));
			ACL acl = new ACL(ZooDefs.Perms.ALL, id);
			acls.add(acl);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return acls;
	}
	
	/**
	 * 
	 * @param ...path
	 * @param value
	 */
	public boolean addProperties(String ...pathValue) {
		boolean result = false;
		if (pathValue != null && pathValue.length > 1) {
			String content = pathValue[pathValue.length - 1];
			String key[] = new String[pathValue.length - 1];
			for(int i = 0; i < key.length; i++) {
				key[i] = pathValue[i];
			}
			String path = getZkPath(key);
			if(!path.endsWith(".properties")) {
				logger.error("The properties file name must end with '.properties'!");
				return result;
			}
			if(ComUtil.isNotEmpty(path) && ComUtil.isNotEmpty(content)) {
				Properties p = new Properties();
				byte[] b = content.getBytes(Constants.defualtCharset);
				try {
					p.load(new ByteArrayInputStream(b));
				} catch (IOException e) {
					e.printStackTrace();
					logger.error("It isnt a valid properties file format.");
					return result;
				}
				
				if(ComUtil.isNotEmpty(Constants.authInfo)) {
					result = ZkOperate.createNode(zk, path, b, getCurrentACLList());
				} else {
					result = ZkOperate.createNode(zk, path, b);
				}
				if(result) {
					fileProperties.put(path, p);
					if(properties != null) {
						CollectionUtils.mergePropertiesIntoMap(p, properties);
					}
				}
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @param ...path
	 * @param value
	 */
	public boolean updateProperties(String ...pathValue) {
		boolean result = false;
		if (pathValue != null && pathValue.length > 1) {
			String content = pathValue[pathValue.length - 1];
			String key[] = new String[pathValue.length - 1];
			for(int i = 0; i < key.length; i++) {
				key[i] = pathValue[i];
			}
			String path = getZkPath(key);
			if(!path.endsWith(".properties")) {
				logger.error("The properties file name must end with '.properties'!");
				return result;
			}
			if(ComUtil.isNotEmpty(path) && ComUtil.isNotEmpty(content)) {
				Properties p = new Properties();
				byte[] b = content.getBytes(Constants.defualtCharset);
				try {
					p.load(new ByteArrayInputStream(b));
				} catch (IOException e) {
					e.printStackTrace();
					logger.error("It isnt a valid properties file format.");
					return result;
				}
				if(ZkOperate.setNodeData(zk, path, b)) {
					fileProperties.put(path, p);
					if(properties != null) {
						removeProperties(fileProperties, path);
						CollectionUtils.mergePropertiesIntoMap(p, properties);
					}
					result = true;
				}
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @param ...path
	 * @param value
	 */
	public boolean deleteProperties(String ...path) {
		boolean result = false;
		if (path != null && path.length > 0) {
			String keyPath = getZkPath(path);
			if(!keyPath.endsWith(".properties")) {
				logger.error("The properties file name must end with '.properties'!");
				return result;
			}
			if(ComUtil.isNotEmpty(keyPath)) {
				if(ZkOperate.deleteNode(zk, keyPath)) {
					removeProperties(fileProperties, keyPath);
					fileProperties.remove(keyPath);
					result = true;
				}
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @param ...path
	 * @param value
	 */
	public boolean addFileValue(String ...pathValue) {
		boolean result = false;
		if (pathValue != null && pathValue.length > 1) {
			String content = pathValue[pathValue.length - 1];
			String key[] = new String[pathValue.length - 1];
			for(int i = 0; i < key.length; i++) {
				key[i] = pathValue[i];
			}
			String path = getZkPath(key);
			if(path.endsWith(".properties")) {
				logger.error("The file name can not end with '.properties'!");
				return result;
			}
			if(ComUtil.isNotEmpty(path) && ComUtil.isNotEmpty(content)) {
				if(ZkOperate.createNode(zk, path, content)) {
					fileValue.put(path, content);
					result = true;
				}
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @param ...path
	 * @param value
	 */
	public boolean updateFileValue(String ...pathValue) {
		boolean result = false;
		if (pathValue != null && pathValue.length > 1) {
			String content = pathValue[pathValue.length - 1];
			String key[] = new String[pathValue.length - 1];
			for(int i = 0; i < key.length; i++) {
				key[i] = pathValue[i];
			}
			String path = getZkPath(key);
			if(path.endsWith(".properties")) {
				logger.error("The file name can not end with '.properties'!");
				return result;
			}
			if(ComUtil.isNotEmpty(path) && ComUtil.isNotEmpty(content)) {
				if(ZkOperate.setNodeData(zk, path, content)) {
					fileValue.put(path, content);
					result = true;
				}
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @param ...path
	 * @param value
	 */
	public boolean deleteFileValue(String ...path) {
		boolean result = false;
		if (path != null && path.length > 0) {
			String keyPath = getZkPath(path);
			if(keyPath.endsWith(".properties")) {
				logger.error("The file name can not end with '.properties'!");
				return result;
			}
			if(ComUtil.isNotEmpty(keyPath)) {
				if(ZkOperate.deleteNode(zk, keyPath)) {
					fileValue.remove(keyPath);
					result = true;
				}
			}
		}
		return result;
	}
	
	public static String getZkPath(String ...path){
		String pathStr = "";
		if (path != null && path.length > 0) {
			for(String p:path) {
				pathStr += ZkCom.getZkPath(p);
			}
		}
		return pathStr;
	}
}
