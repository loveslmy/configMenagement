package com.zk.config.api.test;

import lombok.Data;

@Data
public class Test {
	private String user;
	private String userName;

	public void init() {
		System.out.println("##################user="+user+", userName="+userName);
	}
}
