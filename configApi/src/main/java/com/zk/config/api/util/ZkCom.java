package com.zk.config.api.util;

public class ZkCom {
	public static String getZkPath(String zkPath){
		if("/".equals(zkPath)) {
			return zkPath;
		}
		if (zkPath != null && zkPath.length() > 0) {
			if (!zkPath.startsWith("/")) {
				zkPath = "/" + zkPath;
			}
			if(zkPath.endsWith("/")) {
				zkPath = zkPath.substring(0, zkPath.lastIndexOf("/"));
			}
		}
		return zkPath;
	}
	
	public static String getZkEndPath(String zkPath){
		if("/".equals(zkPath)) {
			return zkPath;
		}
		if (zkPath != null && zkPath.length() > 0) {
			if (!zkPath.startsWith("/")) {
				zkPath = "/" + zkPath;
			}
			if (!zkPath.endsWith("/")) {
				zkPath = zkPath + "/";
			}
		}
		return zkPath;
	}
}
