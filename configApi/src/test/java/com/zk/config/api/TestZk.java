package com.zk.config.api;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.data.Stat;
import org.apache.zookeeper.server.auth.DigestAuthenticationProvider;

public class TestZk {
	private static ZooKeeper zk;
	public static void main(String[] args) throws IOException, KeeperException, InterruptedException, NoSuchAlgorithmException {
		zk = new ZooKeeper("192.168.3.6:2181,192.168.3.7:2181/configManagement/shiyanbijiService/dev",
		        20000, new Watcher() { 
			 		@Override
		            public void process(WatchedEvent event) {
			 			if (event.getState() == KeeperState.Expired) {
							 System.out.println("session expired! zk="+zk);
							if (zk == null || !zk.getState().isConnected()) {
								System.out.println("reconnect!");
								try {
									zk = new ZooKeeper("192.168.3.6:2181,192.168.3.7:2181/configManagement/shiyanbijiService/dev",
									        2000, new Watcher() { 
										 		@Override
									            public void process(WatchedEvent event) {
										 			if (event.getState() == KeeperState.Expired) {
														 System.out.println("session expired!");
													 }
									                System.out.println("watch start " + event.getType() + " ...");
									                System.out.println("Path=" + event.getPath());
									            }
									        });
								} catch (IOException e) {
									e.printStackTrace();
								}
							} else {
								 System.out.println("zk.State" + zk.getState());
							 }
						 }
		                System.out.println("watch start " + event.getType() + " ...");
		                System.out.println("Path=" + event.getPath());
		            }
		        });
//		 zk.create("/testRootPath", "testRootData".getBytes(), Ids.OPEN_ACL_UNSAFE,
//		   CreateMode.PERSISTENT);

//		 zk.create("/testRootPath/testChildPathOne", "testChildDataOne".getBytes(),
//		   Ids.OPEN_ACL_UNSAFE,CreateMode.PERSISTENT);
//		 System.out.println(new String(zk.getData("/testRootPath",false, null)));
//		 System.out.println(zk.getChildren("/testRootPath",true));
//		System.out.println("//testRootPath//abc//".replaceAll("/+", "/").replaceAll("/+$", ""));
////
//		 zk.setData("/testRootPath","".getBytes(),-1);
//		 System.out.println(new String(zk.getData("/testRootPath",false, null)));
//		 System.out.println("zk.exists["+zk.exists("/testRootPath/testChildPathTwo",true)+"]");
//
		 zk.create("/config.properties", "#connectTimeout=3000".getBytes(), 
		   Ids.OPEN_ACL_UNSAFE,CreateMode.PERSISTENT);
//		 System.out.println(new String(zk.getData("/testRootPath/testChildPathTwo",true,new Stat())));
//		 zk.setData("/testRootPath/testChildPathTwo",null,-1);
		 
//		 zk.delete("/testRootPath/testChildPathTwo",-1);
//		 zk.delete("/testRootPath/testChildPathOne",-1);
//
//		 zk.delete("/testRootPath",-1);
		 
//		 zk.create("/solrcloud/task_processtime", String.valueOf(System.currentTimeMillis()).getBytes(), Ids.OPEN_ACL_UNSAFE,
//		   CreateMode.PERSISTENT);
//		Thread.sleep(30000);

//		 List<ACL> acls = new ArrayList<ACL>(2);
//		 Id id1 = new Id("digest", DigestAuthenticationProvider.generateDigest("admin:admin123"));
//		 ACL acl1 = new ACL(ZooDefs.Perms.ALL, id1);
//		 
//		 Id id2 = new Id("digest", DigestAuthenticationProvider.generateDigest("guest:guest123"));
//		 ACL acl2 = new ACL(ZooDefs.Perms.READ, id2);
//		   
//		 acls.add(acl1);
//		 acls.add(acl2);
//		 zk.create("/test", "test".getBytes(), acls, CreateMode.PERSISTENT);
//		 zk.addAuthInfo("digest", "guest:guest".getBytes());
//		 Stat stat = new Stat();
//		 byte[] value = zk.getData("/test", null, stat);
//		 System.out.println(value);
//		Stat stat = zk.exists("/test", false);
//		 System.out.println(stat);
//		List<String> cList = zk.getChildren("/test", false);
//		System.out.println(cList);
//		byte[] b = zk.getData("/configManagement/configWeb/dev/con/test.properties", false, stat);
//		System.out.println(new String(b));
//		zk.delete("/test",-1);
		 zk.close();
	}
}
